<?php
if ($this->session->userdata('tipo_usuario') === "Coordenador") {
    $msg = "Abra o menu Reservas de Lab para editar as reservas.";
} else {
    $msg = "Seus coordenadores farão todo o gerencimento das reservas para você";
}
?>

<div class="alert alert-danger" style="text-align: center">
    <h4><?= $msg ?></h4>
</div>
<script>
    $(document).ready(function () {

    $('#calendar').fullCalendar({
    header: {
    left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
    },
            defaultDate: new Date(),
            navLinks: false, // can click day/week names to navigate views
            businessHours: true,
            editable: true,
            eventLimit: true, // allow "more" link when too many events
            events: [

<?php foreach ($reservas as $r): ?>
                {
                title: '<?= $r->nome_laboratorio . " - Prof.: " . $r->nome_usuario ?>',
                        start: '<?= $r->data . "T" . $r->hora_inicio ?>',
                        end: '<?= $r->data . "T" . $r->hora_inicio ?>',
                        url: '#',
    <?php
    if ($r->nome_status_reserva === 'Aprovada') {
        echo "color: 'green'";
    } else if ($r->nome_status_reserva === 'Pendente') {
        echo "color: 'orange'";
    } else {
        echo "color: 'red'";
    }
    ?>
                },
<?php endforeach; ?>

<?php foreach ($feriados as $f): ?>
                {
                start: new Date().getFullYear() + '-<?= $f->data ?>',
                        end: new Date().getFullYear() + '-<?= $f->data ?>',
                        overlap: false,
                        rendering: 'background',
                        color: '#ff0000'
                },
<?php endforeach; ?>
            ]
    });
    });</script>

<div style="text-align: center; font-size: 15pt">
    <span style="margin-left: 20px; width: 15px; height: 15px; background-color: green; color: green">as</span> Aprovadas
    <span style="margin-left: 20px; width: 15px; height: 15px; background-color: orange; color: orange">as</span> Pendentes
    <span style="margin-left: 20px; width: 15px; height: 15px; background-color: red; color: red">as</span> Canceladas
    <span style="margin-left: 20px; width: 15px; height: 15px; background-color: #ffb3b3; color: #ffb3b3">as</span> Feriados Nacionais
</div><br>
<div id="calendar">

</div>

<link href='<?= base_url('assets/calendario/css/fullcalendar.min.css') ?>' rel='stylesheet' />
<link href='<?= base_url('assets/calendario/css/estilo.css') ?>' rel='stylesheet' />
<link href='<?= base_url('assets/calendario/css/fullcalendar.print.min.css') ?>' rel='stylesheet' media='print' />
<script src='<?= base_url('assets/calendario/js/moment.min.js') ?>'></script>
<script src='<?= base_url('assets/calendario/js/jquery.min.js') ?>'></script>
<script src='<?= base_url('assets/calendario/js/fullcalendar.min.js') ?>'></script>
<script src='<?= base_url('assets/calendario/locale/pt-br.js') ?>'></script>
<script src="<?= base_url('assets/js/requisicoes/reserva.js') ?>"></script>

