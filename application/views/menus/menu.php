<nav class="navbar-mobile">
    <div class="container-fluid">
        <ul class="navbar-mobile__list list-unstyled">
            <?php foreach ($opcoesMenu as $item): ?>
                <li>
                    <a href="<?= $item['link'] ?>">
                        <i class="<?= $item['classes'] ?>"></i><?= $item['menu'] ?>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</nav>
</header>
<!-- END HEADER MOBILE-->

<!-- MENU SIDEBAR-->
<aside class="menu-sidebar d-none d-lg-block">
    <div class="logo">
        <a href="<?= base_url() ?>">
            <img src="<?= base_url('assets/imagens/logo_sisgerlab.png') ?>" alt="Cool Admin" />
        </a>
    </div>
    <div class="menu-sidebar__content js-scrollbar1">
        <nav class="navbar-sidebar">
            <ul class="list-unstyled navbar__list">
                <?php foreach ($opcoesMenu as $item): ?>
                    <li>
                        <a href="<?= $item['link'] ?>">
                            <i class="<?= $item['classes'] ?>"></i><?= $item['menu'] ?>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </nav>
    </div>
</aside>
<!-- END MENU SIDEBAR-->

<!-- PAGE CONTAINER-->
<div class="page-container">
    <!-- HEADER DESKTOP-->
    <header class="header-desktop">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="header-wrap">
                    <form class="form-header" action="" method="POST">

                    </form>


                    <!--Cabeçalho-->
                    <div class="header-button">

                        

                        <!--Conta-->
                        <div class="account-wrap">
                            <div class="account-item clearfix js-item-menu">
                                <div class="image">
                                    <img src="<?= base_url('assets/imagens/usuario.png') ?>" alt="John Doe" />
                                </div>
                                <div class="content">
                                    <a class="js-acc-btn" href="#">
                                        <?php
                                        $nome = explode(' ', $this->session->userdata('nome'));
                                        echo $nome[0];
                                        ?>
                                    </a>
                                </div>
                                <div class="account-dropdown js-dropdown">
                                    <div class="info clearfix">
                                        <div class="image">
                                            <a href="#">
                                                <img src="<?= base_url('assets/imagens/usuario.png') ?>" alt="John Doe" />
                                            </a>
                                        </div>
                                        <div class="content">
                                            <h5 class="name">
                                                <a href="#"><?= $this->session->userdata('nome') ?></a>
                                            </h5>
                                            <span class="email"><?= $this->session->userdata('email') ?></span>
                                        </div>
                                    </div>
                                    <div class="account-dropdown__body">
                                        <div class="account-dropdown__item">
                                            <a href="<?= base_url('Perfil')?>">
                                                <i class="zmdi zmdi-account"></i>Perfil</a>
                                        </div>                                                    
                                    </div>
                                    <div class="account-dropdown__footer">
                                        <a href="<?= base_url('Login/sair') ?>">
                                            <i class="zmdi zmdi-power"></i>Sair
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Fim Conta-->


                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- HEADER DESKTOP-->

    <!-- MAIN CONTENT-->
    <div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="overview-wrap">
                            <h2 class="title-1"><?= $titulo ?></h2>
                        </div>
                    </div>
                </div>

                <!--Conteúdo-->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="au-card recent-report">
                            <div class="au-card-inner">

