<script src="<?= base_url('assets/js/estiloForm.js') ?>"></script>
<script src="<?= base_url('assets/js/requisicoes/laboratorio.js') ?>"></script>

<div class="col col-lg-12" id="btn-novo" style="display: block">
    <button class="btn btn-primary" onclick="ativarForm()">
        Novo Laboratório
        <i class="fa fa-sort-down"></i>
    </button>
    <hr>
</div>


<div class="col col-lg-12" id="form-cad" style="display: none">
    <div style="text-align: center; font-size: 16pt;">
        Novo Laboratório 
    </div>
    <hr>
    <form method="post" id="cad-laboratorio">
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <label for="nome_usuario" class="control-label mb-1">Nome</label>
                    <input id="nome_laboratorio" name="nome" type="text" class="form-control" required="">
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-6">
                <button type="reset" onclick="desativarForm()" class="btn btn-warning btn-block">
                    <span>Cancelar</span>
                </button>
            </div>

            <div class="col-6">
                <button id="payment-button" type="submit" class="btn btn-primary btn-block">
                    <span id="payment-button-amount">Cadastrar</span>
                </button>
            </div>
        </div>
    </form>
    <hr>
</div>


<!--TABELA-->
<div class="col-md-12">
    <!-- DATA TABLE-->
    <div class="table-responsive m-b-40">
        <table class="table table-borderless table-data3">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nome</th>
                    <th>Opções</th>
                </tr>
            </thead>
            <tbody>

                <?php
                $i = 1;
                foreach ($laboratorios as $laboratorio):
                    ?>
                    <tr>
                        <td><?= $i ?></td>
                        <td><?= $laboratorio->nome_laboratorio ?></td>
                        <td>
                            <a class="btn btn-primary" href="<?= base_url('Laboratorio/paginaAlterar/' . $laboratorio->id_laboratorio) ?>">Editar</a>
                            <button  class="btn btn-danger"  href="#" onclick="excluir(<?= $laboratorio->id_laboratorio ?>,'<?= $laboratorio->nome_laboratorio ?>')">Excluir</button>
                        </td>
                    </tr>
                    <?php
                    $i++;
                endforeach;
                ?>
            </tbody>
        </table>

    </div>
    <!-- END DATA TABLE-->
</div>