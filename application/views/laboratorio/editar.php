<script src="<?= base_url('assets/js/requisicoes/laboratorio.js') ?>"></script>

<div class="col col-lg-12" id="btn-novo">
    <a href="<?= base_url('Laboratorio')?>" class="btn btn-primary">
        Voltar
    </a>
    <hr>
</div>


<div class="col col-lg-12" id="form-cad">
    <div style="text-align: center; font-size: 16pt;">
        Editar Laboratório
    </div>
    <hr>
    <form method="post" id="edit-laboratorio">
        <input type="hidden" id="id_laboratorio" value="<?=$laboratorio[0]->id_laboratorio?>">
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <label for="nome_laboratorio" class="control-label mb-1">Nome</label>
                    <input id="nome_laboratorio" value="<?=$laboratorio[0]->nome_laboratorio?>" name="nome_laboratorio" type="text" class="form-control" required="">
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-6">
                <a  href="<?= base_url('Laboratorio')?>" class="btn btn-warning btn-block">
                    <span>Cancelar</span>
                </a>
            </div>
            <div class="col-6">
                <button id="payment-button" type="submit" class="btn btn-primary btn-block">
                    <span id="payment-button-amount">Salvar Alterações</span>
                </button>
            </div>
        </div>
    </form>
    <hr>
</div>