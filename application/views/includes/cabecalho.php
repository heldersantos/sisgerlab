<!DOCTYPE html>
<html lang="pt-br">

    <head>
        <!-- Required meta tags-->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Site de gerenciamento de locação de laboratório">
        <meta name="author" content="Helder dos Santos Sousa">
        <meta name="keywords" content="wordpress sitenotícias">

        <!-- Title Page-->
        <title>SisgerLab</title>

        <!-- Jquery JS-->
        <script src="<?= base_url('assets/template/vendor/jquery-3.2.1.min.js') ?>"></script>

        <!-- Fontfaces CSS-->
        <link href="<?= base_url('assets/template/css/font-face.css') ?>" rel="stylesheet" media="all">
        <link href="<?= base_url('assets/template/vendor/font-awesome-4.7/css/font-awesome.min.css') ?>" rel="stylesheet" media="all">
        <link href="<?= base_url('assets/template/vendor/font-awesome-5/css/fontawesome-all.min.css') ?>" rel="stylesheet" media="all">
        <link href="<?= base_url('assets/template/vendor/mdi-font/css/material-design-iconic-font.min.css') ?>" rel="stylesheet" media="all">

        <!-- Bootstrap CSS-->
        <link href="<?= base_url('assets/template/vendor/bootstrap-4.1/bootstrap.min.css') ?>" rel="stylesheet" media="all">

        <!-- Vendor CSS-->
        <link href="<?= base_url('assets/template/vendor/animsition/animsition.min.css') ?>" rel="stylesheet" media="all">
        <link href="<?= base_url('assets/template/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css') ?>" rel="stylesheet" media="all">
        <link href="<?= base_url('assets/template/vendor/wow/animate.css') ?>" rel="stylesheet" media="all">
        <link href="<?= base_url('assets/template/vendor/css-hamburgers/hamburgers.min.css') ?>" rel="stylesheet" media="all">
        <link href="<?= base_url('assets/template/vendor/slick/slick.css') ?>" rel="stylesheet" media="all">
        <link href="<?= base_url('assets/template/vendor/select2/select2.min.css') ?>" rel="stylesheet" media="all">
        <link href="<?= base_url('assets/template/vendor/perfect-scrollbar/perfect-scrollbar.css') ?>" rel="stylesheet" media="all">

        <!-- Main CSS-->
        <link href="<?= base_url('assets/template/css/theme.css') ?>" rel="stylesheet" media="all">

        <link href="<?= base_url('assets/css/estilo.css') ?>" rel="stylesheet" media="all">

        <link rel="shortcut icon" type="image/png" href="<?= base_url('assets/imagens/logo.png') ?>"/>

        <script src="<?= base_url('assets/js/sweetalert.js') ?>"></script>
        <script src="<?= base_url('assets/js/alerta.js') ?>"></script> 
        <script src="<?= base_url('assets/js/moment.js') ?>"></script> 

        <style>
            ::selection {
                background:#000;
                color: #fff;
            }
        </style>

        <script>
            var base_url = "<?= base_url() ?>";
        </script>

    </head>

    <body class="animsition">
        <div class="page-wrapper">
            <!-- HEADER MOBILE-->
            <header class="header-mobile d-block d-lg-none">
                <div class="header-mobile__bar">
                    <div class="container-fluid">
                        <div class="header-mobile-inner">
                            <a class="logo" href="<?= base_url() ?>">
                                <img src="<?= base_url('assets/imagens/logo_sisgerlab.png') ?>" alt="Sisgerlab" />
                            </a>
                            <button class="hamburger hamburger--slider" type="button">
                                <span class="hamburger-box">
                                    <span class="hamburger-inner"></span>
                                </span>
                            </button>
                        </div>
                    </div>

                </div>

