
<link href='<?= base_url('assets/calendario/css/fullcalendar.min.css') ?>' rel='stylesheet' />
<link href='<?= base_url('assets/calendario/css/estilo.css') ?>' rel='stylesheet' />
<link href='<?= base_url('assets/calendario/css/fullcalendar.print.min.css') ?>' rel='stylesheet' media='print' />
<script src='<?= base_url('assets/calendario/js/moment.min.js') ?>'></script>
<script src='<?= base_url('assets/calendario/js/jquery.min.js') ?>'></script>
<script src='<?= base_url('assets/calendario/js/fullcalendar.min.js') ?>'></script>
<script src='<?= base_url('assets/calendario/locale/pt-br.js') ?>'></script>

<script>
    $(document).ready(function () {

        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay,listWeek'
            },
            defaultDate: new Date(),
            navLinks: true, // can click day/week names to navigate views
            editable: true,
            eventLimit: true, // allow "more" link when too many events
            events: [
                {
                    title: 'Laboratório de informática - Alessandro',
                    start: '2018-11-23T07:00:00',
                    end: '2018-11-23T010:00:00',
                    url:''
                }
            ]
        });

    });
</script>

<div id="calendar">
    
</div>