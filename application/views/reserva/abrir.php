<?php if (isset($erro)): ?>
    <script>
        swal(
        {       title: "Atenção",
                text: "<?= $erro ?>",
                icon: "error",
                button: "Ok", }
        );</script>
<?php endif; ?>

<div class="col col-lg-12" id="form-cad">
    <div class="row">
        <div class="col-lg-12">
            <a class="btn btn-primary" href="<?= base_url('Reserva') ?>">Voltar</a>
        </div>
    </div>
    <br>
    <div class="col-12" id="formulario" style="display:<?= isset($erro) ? "none" : 'block' ?>">
        <form method="post" id="cad-laboratorio">
            <div class="row">
                <div class="col-12">
                    <div class="alert alert-danger">
                        Caso necessário, faça alterações nas datas e horários antes de aprovar.
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label for="nome_usuario" class="control-label mb-1">Professor</label>
                        <input  class="form-control" type="text" value="<?= isset($reserva[0]->nome_usuario) ? $reserva[0]->nome_usuario : '' ?>" disabled="">
                    </div>
                </div>

                <div class="col-6">
                    <div class="form-group">
                        <label for="nome_usuario" class="control-label mb-1">Laboratório</label>
                        <input  class="form-control" type="text" value="<?= isset($reserva[0]->nome_laboratorio) ? $reserva[0]->nome_laboratorio : '' ?>" disabled="">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                    <div class="form-group">
                        <label for="data" class="control-label mb-1">Data</label>
                        <input id="data" name="data" type="date" class="form-control" required="" value="<?= isset($reserva[0]->data) ? $reserva[0]->data : '' ?>">
                    </div>
                </div>

                <div class="col-4">
                    <div class="form-group">
                        <label for="nome_usuario" class="control-label mb-1">De</label>
                        <input id="hora_inicio" name="hora_inicio" type="time" class="form-control" required="" value="<?= isset($reserva[0]->hora_inicio) ? $reserva[0]->hora_inicio : '' ?>">
                    </div>
                </div>

                <div class="col-4">
                    <div class="form-group">
                        <label for="nome_usuario" class="control-label mb-1">Até</label>
                        <input id="hora_fim" name="hora_fim" type="time" class="form-control" required="" value="<?= isset($reserva[0]->hora_fim) ? $reserva[0]->hora_fim : '' ?>">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-6" style="color: #fff">
                    <a href="#" class="btn btn-danger btn-block" onclick="cancelarReserva(<?= isset($reserva[0]->id_reserva) ? $reserva[0]->id_reserva : '' ?>)">Rejeitar Reserva</a>
                </div>

                <div class="col-6" style="color: #fff">
                    <a href="#" onclick="aprovarReserva(<?= isset($reserva[0]->id_reserva) ? $reserva[0]->id_reserva : '' ?>)" type="submit" class="btn btn-success btn-block">Aprovar Reserva</a>
                </div>
            </div>
        </form>
    </div>
    <hr>
    <script>



        $(document).ready(function () {

        $('#calendar').fullCalendar({
        header: {
        left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
        },
                defaultDate: new Date(),
                navLinks: false, // can click day/week names to navigate views
                businessHours: true,
                editable: true,
                eventLimit: true, // allow "more" link when too many events
                events: [

<?php foreach ($reservas as $r): ?>
                    {
                    title: '<?= $r->nome_laboratorio . " - Prof.: " . $r->nome_usuario ?>',
                            start: '<?= $r->data . "T" . $r->hora_inicio ?>',
                            end: '<?= $r->data . "T" . $r->hora_inicio ?>',
                            url: '<?= base_url('Reserva/abrir/' . $r->id_reserva) ?>',
    <?php
    if ($r->nome_status_reserva === 'Aprovada') {
        echo "color: 'green'";
    } else if ($r->nome_status_reserva === 'Pendente') {
        echo "color: 'orange'";
    } else {
        echo "color: 'red'";
    }
    ?>
                    },
<?php endforeach; ?>

<?php foreach ($feriados as $f): ?>
                    {
                    start: new Date().getFullYear() + '-<?= $f->data ?>',
                            end: new Date().getFullYear() + '-<?= $f->data ?>',
                            overlap: false,
                            rendering: 'background',
                            color: '#ff0000'
                    },
<?php endforeach; ?>
                ]
        });
        });
    </script>


    <div class="col-12">
        <div class="row" style="text-align: center; border: 1px solid #333;">

            <div id="titulo-agenda">Confira a Agenda de Reservas</div>

            <div style="text-align: center; font-size: 15pt">
                <span style="margin-left: 20px; width: 15px; height: 15px; background-color: green; color: green">as</span> Aprovadas
                <span style="margin-left: 20px; width: 15px; height: 15px; background-color: orange; color: orange">as</span> Pendentes
                <span style="margin-left: 20px; width: 15px; height: 15px; background-color: red; color: red">as</span> Canceladas
                <span style="margin-left: 20px; width: 15px; height: 15px; background-color: #ffb3b3; color: #ffb3b3">as</span> Feriados Nacionais
            </div>

            <div id="calendar" style="margin: 15px;">

            </div>
        </div>
    </div>
    <br>




</div>

<link href='<?= base_url('assets/calendario/css/fullcalendar.min.css') ?>' rel='stylesheet' />
<link href='<?= base_url('assets/calendario/css/estilo.css') ?>' rel='stylesheet' />
<link href='<?= base_url('assets/calendario/css/fullcalendar.print.min.css') ?>' rel='stylesheet' media='print' />
<script src='<?= base_url('assets/calendario/js/moment.min.js') ?>'></script>
<script src='<?= base_url('assets/calendario/js/jquery.min.js') ?>'></script>
<script src='<?= base_url('assets/calendario/js/fullcalendar.min.js') ?>'></script>
<script src='<?= base_url('assets/calendario/locale/pt-br.js') ?>'></script>
<script src="<?= base_url('assets/js/requisicoes/reserva.js') ?>"></script>


