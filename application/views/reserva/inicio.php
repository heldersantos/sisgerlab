<div class="col col-lg-12" id="form-cad">
    <form action="<?= base_url('Reserva/buscarPorStatus')?>" method="post">
        <div class="row">
            <div class="col-12">
                <label for="id_status_reserva" class="control-label mb-1">Buscar</label>
            </div>
            <div class="col-md-12">
                <div class="form-group">      
                    <div class="input-group">
                        <select id="id_status_reserva" name="id_status_reserva" class="form-control">
                            <option value="" <?= $opcao==""?"Selected":"" ?>>Todos os tipos</option>
                            <option value="1" <?= $opcao=="1"?"Selected":"" ?>>Aprovadas</option>
                            <option value="2" <?= $opcao=="2"?"Selected":"" ?>>Pendentes</option>
                            <option value="3" <?= $opcao=="3"?"Selected":"" ?>>Canceladas</option>
                        </select>
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-primary">Buscar</button>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </form>
</div>

<!--TABELA-->
<div class="col-md-12">
    <!-- DATA TABLE-->
    <div class="table-responsive m-b-40">
        <table class="table table-borderless table-data3">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Professor</th>
                    <th>Laboratório</th>
                    <th>Data</th>
                    <th>Situação</th>
                    <th>Abrir</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 1;
                foreach ($reservas as $reserva):
                    ?>
                    <tr>
                        <td><?= $i ?></td>
                        <td><?= $reserva->nome_usuario ?></td>
                        <td><?= $reserva->nome_laboratorio ?></td>
                        <td><?= converterDataUsuario($reserva->data) ?></td>
                        <td style="font-weight: bold; color: <?php
                            if ($reserva->nome_status_reserva === "Aprovada") {
                                echo "green";
                            } else if ($reserva->nome_status_reserva === "Pendente") {
                                echo "orange";
                            } else {
                                echo "red";
                            }
                            ?>">  
                                <?= $reserva->nome_status_reserva ?>
                        </td>
                        <td>
                            <a class="btn btn-primary" href="<?= base_url('Reserva/abrir/'.$reserva->id_reserva)?>">Abrir</a>
                        </td>
                    </tr>
                    <?php
                    $i++;
                endforeach;
                ?>
            </tbody>
        </table>

    </div>
    <!-- END DATA TABLE-->
</div>

