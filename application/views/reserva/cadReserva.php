<script src="<?= base_url('assets/js/estiloForm.js') ?>"></script>
<script src="<?= base_url('assets/js/requisicoes/reserva.js') ?>"></script>

<div class="col col-lg-12" id="btn-novo" style="display: block">
    <button class="btn btn-primary" onclick="ativarForm()">
        Solicitar Nova Reserva
        <i class="fa fa-sort-down"></i>
    </button>
    <hr>
</div>


<div class="col col-lg-12" id="form-cad" style="display: none">
    <div style="text-align: center; font-size: 16pt;">
        Novo Solicitação de Reserva
    </div>
    <hr>
    <form method="post" id="cad-reserva">
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <label for="id_coordenador" class="control-label mb-1">Seu Coordenador</label>
                    <select id="id_coordenador" name="id_coordenador" class="form-control" required="">
                        <option value="">Selecione</option>
                        <?php foreach ($coord as $c): ?>
                            <option value="<?= $c->id_usuario ?>"><?= $c->nome_usuario ?></option>                        
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            
            <div class="col-3">
                <div class="form-group">
                    <label for="id_laboratorio" class="control-label mb-1">Laboratório</label>
                    <select id="id_laboratorio" name="id_laboratorio" class="form-control" required="">
                        <option value="">Selecione</option>
                        <?php foreach ($laboratorios as $l): ?>
                            <option value="<?= $l->id_laboratorio ?>"><?= $l->nome_laboratorio ?></option>                        
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>

            <div class="col-3">
                <div class="form-group">
                    <label for="data" class="control-label mb-1">Data</label>
                    <input id="data" name="data" type="date" class="form-control" required="">
                </div>
            </div>

            <div class="col-3">
                <div class="form-group">
                    <label for="nome_usuario" class="control-label mb-1">De</label>
                    <select id="hora_inicio" name="hora_inicio" class="form-control" required="">
                        <option value="">Selecione</option>
                        <?php for ($i = 7; $i <= 22; $i++): ?>
                            <option value="<?= $i < 10 ? '0' . $i : $i ?>:00:00"><?= $i < 10 ? '0' . $i : $i ?>:00</option>                        
                        <?php endfor; ?>
                    </select>
                </div>
            </div>

            <div class="col-3">
                <div class="form-group">
                    <label for="nome_usuario" class="control-label mb-1">Até</label>
                    <select id="hora_fim" name="hora_fim" class="form-control" required="">
                        <option value="">Selecione</option>
                        <?php for ($i = 7; $i <= 22; $i++): ?>
                            <option value="<?= $i < 10 ? '0' . $i : $i ?>:00:00"><?= $i < 10 ? '0' . $i : $i ?>:00</option>                        
                        <?php endfor; ?>
                    </select>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-6">
                <button type="reset" onclick="desativarForm()" class="btn btn-warning btn-block">
                    <span>Cancelar</span>
                </button>
            </div>

            <div class="col-6">
                <button id="payment-button" type="submit" class="btn btn-primary btn-block">
                    <span id="payment-button-amount">Solicitar Reserva</span>
                </button>
            </div>
        </div>
    </form>
    <hr>
</div>


<!--TABELA-->
<div class="col-md-12">
    <!-- DATA TABLE-->
    <center>
        <h3>Suas Reservas</h3>
    </center>
    <div class="table-responsive m-b-40">
        <table class="table table-borderless table-data3" style="text-align: center">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Laboratorio</th>
                    <th>Data</th>
                    <th>Situação</th>
                    <th>Motivo Cancelamento</th>
                    <th>Excluir</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 1;
                foreach ($reservas as $reserva):
                    ?>
                    <tr>
                        <td><?= $i ?></td>
                        <td><?= $reserva->nome_laboratorio ?></td>
                        <td><?= converterDataUsuario($reserva->data) . "<br>" . "De " . $reserva->hora_inicio . " as " . $reserva->hora_fim ?></td>
                        <td style="font-weight: bold; color: <?php
                        if ($reserva->nome_status_reserva === "Aprovada") {
                            echo "green";
                        } else if ($reserva->nome_status_reserva === "Pendente") {
                            echo "orange";
                        } else {
                            echo "red";
                        }
                        ?>">  
                                <?= $reserva->nome_status_reserva == 'Pendente' ? 'Encaminhada' : $reserva->nome_status_reserva ?>
                        </td>
                        <td><?= $reserva->resposta ?></td>
                        <td>
                            <button class = "btn btn-danger" onclick = "excluirReserva('<?= base_url('Reserva/excluir/' . $reserva->id_reserva) ?>')">Excluir</button>
                        </td>
                    </tr>
                    <?php
                    $i++;
                endforeach;
                ?>
            </tbody>
        </table>

    </div>
    <!-- END DATA TABLE-->
</div>

