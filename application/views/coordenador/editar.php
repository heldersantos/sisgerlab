<script src="<?= base_url('assets/js/requisicoes/coordenador.js') ?>"></script>

<div class="col col-lg-12" id="btn-novo">
    <a href="<?= base_url('Coordenador')?>" class="btn btn-primary">
        Voltar
    </a>
    <hr>
</div>


<div class="col col-lg-12" id="form-cad">
    <div style="text-align: center; font-size: 16pt;">
        Editar Coordenador
    </div>
    <hr>
    <form method="post" id="edit-coordenador">
        <input type="hidden" id="id_usuario" value="<?=$coordenador[0]->id_usuario?>">
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <label for="nome_usuario" class="control-label mb-1">Nome</label>
                    <input id="nome_usuario" value="<?=$coordenador[0]->nome_usuario?>" name="nome" type="text" class="form-control" required="">
                </div>
            </div>

            <div class="col-12">
                <div class="form-group">
                    <label for="login" class="control-label mb-1">E-mail</label>
                    <input id="login" value="<?=$coordenador[0]->login?>" name="login" type="email" class="form-control cc-exp" required="">
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-6">
                <a  href="<?= base_url('Coordenador')?>" class="btn btn-warning btn-block">
                    <span>Cancelar</span>
                </a>
            </div>

            <div class="col-6">
                <button id="payment-button" type="submit" class="btn btn-primary btn-block">
                    <span id="payment-button-amount">Salvar Alterações</span>
                </button>
            </div>
        </div>
    </form>
    <hr>
</div>