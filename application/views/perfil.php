<style>

    #links-perfil{
        margin: -40px -40px 20px -40px;
        height: 50px; 
        border-bottom: 2px solid #3f4853;
        background-color: #ccc;
    }

    #links-perfil a{
        padding: 15px 40px 33px 40px;
        color: #333;
        border-radius: 10px 10px 0 0;
    }

    #links-perfil a.selecionado{
        border-bottom: 2px solid #fff;
        color: #333;
        background-color: #fff;
    }

    #links-perfil a.naoselecionado{
        border-bottom: 2px solid #fff;
        color: #333;
        background-color: #ccc;
        border-bottom: 2px solid #3f4853;
    }

    #links-perfil a:hover{
        color: #007bff;
    }

    .formulario{
        display: none;
    }

    .visivel{
        display: block;
    }
</style>

<script src="<?= base_url('assets/js/requisicoes/perfil.js') ?>"></script>

<div class="row" id="links-perfil">
    <a href="#" class="selecionado" onclick="mudar('senha')">Perfil</a>
    <a href="#" class="naoselecionado" onclick="mudar('dados')">Senha</a>
</div>

<div id="dados" class="row formulario">
    <div class="col-12">
        <form id="form-perfil" method="post" action="<?= base_url('Perfil/alterarPerfil') ?>">
            <div class="row">
                <div class="col-6">
                    <div class="form-group">
                        <label for="nome_usuario">Nome</label>
                        <input id="nome_usuario" autofocus="" class="form-control" type="text" name="nome_usuario" required="" value="<?= $this->session->userdata('nome') ?>"> 
                    </div>
                </div>

                <div class="col-6">
                    <div class="form-group">
                        <label for="login">E-mail</label>
                        <input id="login" class="form-control" type="email" name="login" required="" value="<?= $this->session->userdata('email') ?>"> 
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-6"></div>
                <div class="col-3">
                    <a class="btn btn-warning btn-block" href="<?= base_url() ?>">Cancelar</a>
                </div>
                <div class="col-3">
                    <button class="btn btn-block btn-primary">Salvar Alterações</button>
                </div>
            </div>
        </form>
    </div>

</div>


<div id="senha" class="row formulario">
    <div class="col-md-12">
        <form id="form-senha">
            <div class="row">
                <div class="col-4">
                    <div class="form-group">
                        <label for="senha_atual">Senha Atual</label>
                        <input id="senha_atual" autofocus="" class="form-control" type="password" name="senha_atual" required=""> 
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label for="nova_senha">Nova Senha</label>
                        <input id="nova_senha" autofocus="" class="form-control" type="password" name="nova_senha" required=""> 
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label for="confirmar">Confirmar Senha</label>
                        <input id="confirmar" autofocus="" class="form-control" type="password" name="confirmar" required=""> 
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-6"></div>
                <div class="col-3">
                    <a class="btn btn-warning btn-block" href="<?= base_url() ?>">Cancelar</a>
                </div>
                <div class="col-3">
                    <button class="btn btn-block btn-primary">Salvar Alterações</button>
                </div>
            </div>
        </form>
    </div>

</div>

