<!DOCTYPE html>
<html lang="en">

    <head>
        <!-- Required meta tags-->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Site de gerenciamento de locação de laboratório">
        <meta name="author" content="Helder dos Santos Sousa">
        <meta name="keywords" content="wordpress sitenotícias">

        <!-- Title Page-->
        <title>SisgerLab</title>

        <!-- Fontfaces CSS-->
        <link href="<?= base_url('assets/template/css/font-face.css') ?>" rel="stylesheet" media="all">
        <link href="<?= base_url('assets/template/vendor/font-awesome-4.7/css/font-awesome.min.css') ?>" rel="stylesheet" media="all">
        <link href="<?= base_url('assets/template/vendor/font-awesome-5/css/fontawesome-all.min.css') ?>" rel="stylesheet" media="all">
        <link href="<?= base_url('assets/template/vendor/mdi-font/css/material-design-iconic-font.min.css') ?>" rel="stylesheet" media="all">

        <link rel="shortcut icon" type="image/png" href="<?= base_url('assets/imagens/logo.png') ?>"/>

        <!-- Bootstrap CSS-->
        <link href="<?= base_url('assets/template/vendor/bootstrap-4.1/bootstrap.min.css') ?>" rel="stylesheet" media="all">

        <!-- Vendor CSS-->
        <link href="<?= base_url('assets/template/vendor/animsition/animsition.min.css') ?>" rel="stylesheet" media="all">
        <link href="<?= base_url('assets/template/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css') ?>" rel="stylesheet" media="all">
        <link href="<?= base_url('assets/template/vendor/wow/animate.css') ?>" rel="stylesheet" media="all">
        <link href="<?= base_url('assets/template/vendor/css-hamburgers/hamburgers.min.css') ?>" rel="stylesheet" media="all">
        <link href="<?= base_url('assets/template/vendor/slick/slick.css') ?>" rel="stylesheet" media="all">
        <link href="<?= base_url('assets/template/vendor/select2/select2.min.css') ?>" rel="stylesheet" media="all">
        <link href="<?= base_url('assets/template/vendor/perfect-scrollbar/perfect-scrollbar.css') ?>" rel="stylesheet" media="all">

        <!-- Main CSS-->
        <link href="<?= base_url('assets/template/css/theme.css') ?>" rel="stylesheet" media="all">

        <script>
            var base_url = "<?= base_url() ?>";
        </script>

        <style>
            #fundo{
                background: #890199; /* Old browsers */
                background: -moz-linear-gradient(45deg, #890199 0%, #002a8c 100%); /* FF3.6-15 */
                background: -webkit-linear-gradient(45deg, #890199 0%,#002a8c 100%); /* Chrome10-25,Safari5.1-6 */
                background: linear-gradient(45deg, #890199 0%,#002a8c 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#890199', endColorstr='#002a8c',GradientType=1 );
            }
        </style>

    </head>

    <body class="animsition">
        <div class="page-wrapper">
            <div class="page-content--bge5" id="fundo">
                <div class="container">
                    <div class="login-wrap">
                        <div class="login-content">
                            <div class="login-logo">
                                <a href="#">
                                    <img src="<?= base_url('assets/imagens/logo_sisgerlab.png') ?>" alt="SisgerLab">
                                </a>
                            </div>
                            <div class="login-form">
                                <form id="form-login" action="<?= base_url('Login/entrar') ?>" method="post">
                                    <div class="form-group">
                                        <label>Usuário</label>
                                        <input id="login" class="au-input au-input--full" type="email" name="login" autofocus="">
                                    </div>
                                    <div class="form-group">
                                        <label>Senha</label>
                                        <input id="senha" class="au-input au-input--full" type="password" name="senha">
                                    </div>

                                    <button class="au-btn au-btn--block au-btn--blue m-b-20" type="submit">ENTRAR</button>

                                    <div class="login-checkbox">
                                        <label>
                                            <!--<a href="#">Esqueceu sua senha?</a>-->
                                        </label>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <!-- Jquery JS-->
        <script src="<?= base_url('assets/template/vendor/jquery-3.2.1.min.js') ?>"></script>
        <!-- Bootstrap JS-->
        <script src="<?= base_url('assets/template/vendor/bootstrap-4.1/popper.min.js') ?>"></script>
        <script src="<?= base_url('assets/template/vendor/bootstrap-4.1/bootstrap.min.js') ?>"></script>
        <!-- Vendor JS       -->
        <script src="<?= base_url('assets/template/vendor/slick/slick.min.js') ?>">
        </script>
        <script src="<?= base_url('assets/template/vendor/wow/wow.min.js') ?>"></script>
        <script src="<?= base_url('assets/template/vendor/animsition/animsition.min.js') ?>"></script>
        <script src="<?= base_url('assets/template/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js') ?>">
        </script>
        <script src="v<?= base_url('assets/template/endor/counter-up/jquery.waypoints.min.js') ?>"></script>
        <script src="<?= base_url('assets/template/vendor/counter-up/jquery.counterup.min.js') ?>">
        </script>
        <script src="<?= base_url('assets/template/vendor/circle-progress/circle-progress.min.js') ?>"></script>
        <script src="<?= base_url('assets/template/vendor/perfect-scrollbar/perfect-scrollbar.js') ?>"></script>
        <script src="<?= base_url('assets/template/vendor/chartjs/Chart.bundle.min.js') ?>"></script>
        <script src="<?= base_url('assets/template/vendor/select2/select2.min.js') ?>">
        </script>

        <!-- Main JS-->
        <script src="<?= base_url('assets/template/js/main.js') ?>"></script>
        <script src="<?= base_url('assets/js/alerta.js') ?>"></script>
        <script src="<?= base_url('assets/js/requisicoes/login.js') ?>"></script>

    </body>

</html>
<!-- end document-->