<!DOCTYPE html>
<html lang="pt-br">

    <head>
        <!-- Required meta tags-->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Site de gerenciamento de locação de laboratório">
        <meta name="author" content="Helder dos Santos Sousa">
        <meta name="keywords" content="wordpress sitenotícias">

        <!-- Title Page-->
        <title>SisgerLab - Erro 404</title>

        <link rel="shortcut icon" type="image/png" href="<?= base_url('assets/imagens/logo.png') ?>"/>

        <!-- Jquery JS-->
        <script src="<?= base_url('assets/template/vendor/jquery-3.2.1.min.js') ?>"></script>
        <!-- Bootstrap CSS-->
        <link href="<?= base_url('assets/template/vendor/bootstrap-4.1/bootstrap.min.css') ?>" rel="stylesheet" media="all">

        <style type="text/css">
            #fundo{
                background-image: url("<?= base_url('assets/imagens/error-bg.jpg') ?>");
                background-size: cover;
                height: 100%;
                background-position: center;
                display: flex;
                flex-direction: row;
                justify-content: center;
                align-items: center
            }

            html, body{
                height: 100%;
            }

            #texto{
                text-align: center;

            }

            h1{
                font-size: 150pt;
                font-weight: 1000;
            }
            
            #msg{
                font-size: 25pt;
                margin-bottom: 50px;
            }
        </style>

    </head>

    <body class="animsition">

        <div id="fundo">
            <div id="texto">
                <h1>404</h1>
                <div id="msg">
                    PÁGINA NÃO ENCONTRADA
                </div>

                <div id="botao">
                    <a href="<?= base_url() ?>" class="btn btn-primary btn-block">VOLTAR AO INÍCIO</a>
                </div>
            </div>
        </div>

    </body>

</html>