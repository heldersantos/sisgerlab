<script src="<?= base_url('assets/js/estiloForm.js') ?>"></script>
<script src="<?= base_url('assets/js/requisicoes/professor.js') ?>"></script>

<?php if ($this->session->userdata('tipo_usuario') === "Coordenador"): ?>
    <div class="col col-lg-12" id="btn-novo" style="display: block">
        <button class="btn btn-primary" onclick="ativarForm()">
            Novo Professor
            <i class="fa fa-sort-down"></i>
        </button>
        <hr>
    </div>


    <div class="col col-lg-12" id="form-cad" style="display: none">
        <div style="text-align: center; font-size: 16pt;">
            Novo Professor 
        </div>
        <hr>
        <form method="post" id="cad-professor">
            <input type="hidden" id="nome_tipo_usuario" value="<?= $this->session->userdata('tipo_usuario') ?>">
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <label for="nome_usuario" class="control-label mb-1">Nome</label>
                        <input id="nome_usuario" name="nome" type="text" class="form-control" required="">
                    </div>
                </div>

                <div class="col-12">
                    <div class="form-group">
                        <label for="login" class="control-label mb-1">E-mail</label>
                        <input id="login" name="login" type="email" class="form-control cc-exp" required="">
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-6">
                    <label for="senha" class="control-label mb-1">Senha</label>
                    <div class="input-group">
                        <input minlength="8" maxlength="15" id="senha" name="senha" type="password" class="form-control cc-cvc" required="">
                    </div>
                </div>

                <div class="col-6">
                    <label for="confirmaSenha" class="control-label mb-1">Confirmar Senha</label>
                    <div class="input-group">
                        <input minlength="8" maxlength="15" id="confirmaSenha" name="confirmaSenha" type="password" class="form-control cc-cvc" required="">
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-6">
                    <button type="reset" onclick="desativarForm()" class="btn btn-warning btn-block">
                        <span>Cancelar</span>
                    </button>
                </div>

                <div class="col-6">
                    <button id="payment-button" type="submit" class="btn btn-primary btn-block">
                        <span id="payment-button-amount">Cadastrar</span>
                    </button>
                </div>
            </div>
        </form>
        <hr>
    </div>
<?php endif; ?>

<!--TABELA-->
<div class="col-md-12">
    <!-- DATA TABLE-->
    <div class="table-responsive m-b-40">
        <table class="table table-borderless table-data3">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nome</th>
                    <th>E-amail</th>

                    <?php if ($this->session->userdata('tipo_usuario') === "Coordenador"): ?>
                        <th>Opções</th>
                    <?php endif; ?>
                </tr>
            </thead>
            <tbody>

                <?php
                $i = 1;
                foreach ($professores as $professor):
                    ?>
                    <tr>
                        <td><?= $i ?></td>
                        <td><?= $professor->nome_usuario ?></td>
                        <td><?= $professor->login ?></td>

                        <?php if ($this->session->userdata('tipo_usuario') === "Coordenador"): ?>
                            <td>
                                <a class="btn btn-primary" href="<?= base_url('Professor/paginaAlterar/' . $professor->id_usuario) ?>">Editar</a>
                                <button  class="btn btn-danger"  href="#" onclick="excluir(<?= $professor->id_usuario ?>, '<?= $professor->nome_usuario ?>')">Excluir</button>
                            </td>
                        <?php endif; ?>
                    </tr>
                    <?php
                    $i++;
                endforeach;
                ?>
            </tbody>
        </table>

    </div>
    <!-- END DATA TABLE-->
</div>