<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'application/controllers/Controller.php';

/**
 * Controlador responsável por receber as requisições referentes ao carregamento de páginas
 * referentes a dados dos professores e de realização do CRUD de professores
 * 
 * @author Helder dos Santos
 */
class Professor extends Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('UsuarioModel', 'usuario');
    }

    public function alterar() {
        $this->verificar_permissao(array('Coordenador'));

        $result = $this->usuario->buscarPorLoginEditar(
                $this->input->post('login'), $this->input->post('id_usuario')
        );

        $dados_view = array('status' => 'erro', 'msg' => 'Erro ao alterar Professor');

        if (count($result) === 0) {
            $coordenador = array(
                'nome_usuario' => $this->input->post('nome_usuario'),
                'login' => $this->input->post('login')
            );
            if ($this->usuario->alterar($this->input->post('id_usuario'), $coordenador)) {
                $dados_view = array('status' => 'ok', 'msg' => 'Professor alterado com sucesso!');
            }
        } else {
            $dados_view = array('status' => 'erro', 'msg' => 'Este e-mail já é cadastrado!');
        }

        echo json_encode($dados_view);
    }

    public function buscar($id) {
         redirect(base_url());
    }

    public function buscarTodos() {
         redirect(base_url());
    }

    public function cadastrar() {
        $this->verificar_permissao(array('Coordenador'));
        $professor = array(
            'nome_usuario' => $this->input->post('nome_usuario'),
            'login' => $this->input->post('login'),
            'senha' => password_hash($this->input->post('senha'), PASSWORD_DEFAULT),
            'id_tipo_usuario' => 1
        );

        $dadosView = array('status' => 'erro', 'msg' => 'Erro ao cadastrar professor!');

        if (count($this->usuario->buscarPorLogin($this->input->post('login'))) == 0) {
            $resposta = $this->usuario->cadastrar($professor);

            if ($resposta === true) {
                $dadosView = array('status' => 'ok', 'msg' => 'Professor cadastrado com sucesso!');
            }
        } else {
            $dadosView = array('status' => 'erro', 'msg' => 'Este e-mail já é cadastrado!');
        }

        echo json_encode($dadosView);
    }

    public function excluir($id) {
        $this->verificar_permissao(array('Coordenador'));
        $dados_view = array("status" => 'erro', "msg" => 'Erro ao excluir professor');

        if ($this->usuario->excluir($id)) {
            $dados_view = array("status" => 'ok', "msg" => 'Professor excluído com sucesso!');
        }
        echo json_encode($dados_view);
    }

    public function index() {
        $this->verificar_permissao(array('Coordenador'));
        $dados_view['titulo'] = "Professores";
        $dados_view['professores'] = $this->usuario->buscarTodosProfessores();
        $this->carregarPagina('professor/cadastro', $dados_view);
    }

    public function paginaAlterar($id) {
        $this->verificar_permissao(array('Coordenador'));
        $dados_view['titulo'] = "Professores";
        $dados_view['coordenadores'] = $this->usuario->buscarTodosCoordenadores();
        $dados_view['professor'] = $this->usuario->buscarPorId($id);
        $this->carregarPagina('professor/editar', $dados_view);
    }

    public function paginaCadastrar() {
        redirect(base_url());
    }

}
