<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Classe abstrata que é herdada por todas as classes controladores
 * e estas devem implementar os métodos abstratos contidos nesta classe
 *
 * @author Helder dos Santos
 */
abstract class Controller extends CI_Controller {

    abstract function index();

    abstract function paginaCadastrar();

    abstract function paginaAlterar($id);

    abstract function buscarTodos();

    abstract function buscar($id);

    abstract function alterar();

    abstract function cadastrar();

    abstract function excluir($id);

    /**
     * método responsável por fazer o carregamentos das telas dos sistema 
     * e passar os dados necessários a cada um dos arquivos carregados
     * @param type $view
     * @param array $dados_view
     */
    protected function carregarPagina($view, $dados_view) {
        $dados_view['opcoesMenu'] = $this->carregarMenu();
        $this->load->view('includes/cabecalho');
        $this->load->view('menus/menu', $dados_view);
        $this->load->view($view, $dados_view);
        $this->load->view('includes/rodape');
    }

    /**
     * Carrega as opções de menu de acordo com o nível acesso do usuário do usuário
     * @return array
     */
    public function carregarMenu() {
        $dadosSessao = $this->session->userdata('tipo_usuario');
        $menu = array();
        if ($dadosSessao === 'Administrador') {
            $menu = array(
                array('classes' => 'fas fa-tachometer-alt', 'menu' => 'Início', 'link' => base_url()),
                array('classes' => 'fas fa-user', 'menu' => 'Professores', 'link' => base_url('Professor')),
                array('classes' => 'fas fa-user', 'menu' => 'Coordenadores', 'link' => base_url('Coordenador')),
                array('classes' => 'fas fa-home', 'menu' => 'Laboratórios', 'link' => base_url('Laboratorio'))
            );
        } else if ($dadosSessao == 'Coordenador') {
            $menu = array(
                array('classes' => 'fas fa-tachometer-alt', 'menu' => 'Início', 'link' => base_url()),
                array('classes' => 'fas fa-user', 'menu' => 'Professores', 'link' => base_url('Professor')),
                array('classes' => 'zmdi zmdi-comment-more', 'menu' => 'Reservas de Lab.', 'link' => base_url('Reserva'))
            );
        } else if ($dadosSessao == 'Professor') {
            $menu = array(
                array('classes' => 'fas fa-tachometer-alt', 'menu' => 'Início', 'link' => base_url())
            );
        }
        return $menu;
    }

    /**
     * recebe os dados da sessão criada e um array de níveis de 
     * acesso permitidos e verifica se o nível da sessão tem permissão para acessar 
     * o conteúdo da página
     * @param type $sessao
     * @param type $niveis_permitidos
     */
    public function verificar_permissao($niveis_permitidos) {
        $acesso_permitido = false;
        $sessao = $this->session->userdata();
        if (isset($sessao['logado'])) {
            foreach ($niveis_permitidos as $valor) {
                if ($sessao['tipo_usuario'] == $valor) {
                    $acesso_permitido = true;
                }
            }
        }

        if ($acesso_permitido == false) {
            redirect(base_url('Login'));
        }
    }

}
