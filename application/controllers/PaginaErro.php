<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
 * Carrega uma view para mostrar uma página de erro personaliza
 *
 * @author Helder dos Santos
 */
class PaginaErro extends CI_Controller{
    public function index(){
        $this->load->view('erro404');
    }
}
