<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'application/controllers/Controller.php';

/**
 * Classe responsável por abrir a janela inicial do sistema de cada
 * nível de usuário que esteja logado, caso este não esteja logado será 
 * redirecionado para o login do sistema
 *
 * @author Helder dos Santos
 */
class Inicio extends Controller {

    /**
     * Faz o carregamento dos Models utilizados nesta classe
     */
    public function __construct() {
        parent::__construct();
        $this->verificar_permissao(array('Administrador', 'Professor', 'Coordenador'));
        $this->load->model('ReservaModel', 'reserva');
        $this->load->model('UsuarioModel', 'usuario');
        $this->load->model('LaboratorioModel', 'laboratorio');
    }

    public function index() {
        $tipoUsuario = $this->session->userdata('tipo_usuario');

        if ($tipoUsuario === "Professor") {
            $this->paginaProf();
        } else {
            $this->paginaInicio();
        }
    }

    private function paginaInicio() {
        $dados_view['titulo'] = 'Agenda de Reservas de Laboratório';
        $dados_view['reservas'] = $this->reserva->buscarTodas();
        $dados_view['feriados'] = $this->reserva->buscarFeriados();
        $this->carregarPagina('inicio', $dados_view);
    }

    private function paginaProf() {
        $dados_view['titulo'] = "Solicitações de Reservas";
        $dados_view['laboratorios'] = $this->laboratorio->buscarTodos();
        $dados_view['reservas'] = $this->reserva->buscarTodasProfessor();
        $dados_view['coord'] = $this->usuario->buscarTodosCoordenadores();
        $this->carregarPagina('reserva/cadReserva', $dados_view);
    }

    public function alterar() {
        
    }

    public function buscar($id) {
        redirect(base_url());
    }

    public function buscarTodos() {
        redirect(base_url());
    }

    public function cadastrar() {
        redirect(base_url());
    }

    public function excluir($id) {
        redirect(base_url());
    }

    public function paginaAlterar($id) {
        redirect(base_url());
    }

    public function paginaCadastrar() {
        redirect(base_url());
    }

}
