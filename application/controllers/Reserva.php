<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'application/controllers/Controller.php';

/**
 * Controlador responsável por receber as requisições referentes ao carregamento de páginas
 * referentes a dados das reservas e de realização do CRUD de reservas
 * 
 * @author Helder dos Santos
 */
class Reserva extends Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('ReservaModel', 'reserva');
        $this->load->model('UsuarioModel', 'usuario');
        $this->load->model('LaboratorioModel', 'laboratorio');
    }

    public function aprovar() {
        $this->verificar_permissao(array('Coordenador'));
        $dados_view = array('status' => 'erro', 'msg' => 'Erro ao aprovar reserva. Tente novamente');

        if (!$this->isFeriado($this->input->post('data'))) {
            if (!$this->isFinalSemana($this->input->post('data'))) {
                $reserva = array(
                    'data' => $this->input->post('data'),
                    'hora_inicio' => $this->input->post('hora_inicio'),
                    'hora_fim' => $this->input->post('hora_fim'),
                    'id_status_reserva' => 1
                );

                if ($this->reserva->alterar($reserva, $this->input->post('id_reserva'))) {
                    $dados_view = array('status' => 'ok', 'msg' => 'Reserva aprovada com sucesso.');
                }
            } else {
                $dados_view = array('status' => 'erro', 'msg' => 'Não é permitido reserva aos domingos.');
            }
        } else {
            $dados_view = array('status' => 'erro', 'msg' => 'Essa data é feriado.');
        }

        echo json_encode($dados_view);
    }

    private function isFeriado($data) {
        $this->load->model('FeriadoModel', 'feriado');
        $data = explode('-', $data);
        $data = $data[1] . "-" . $data[2];

        if (count($this->feriado->buscarPorData($data)) > 0) {
            return true;
        }

        return false;
    }

    private function isFinalSemana($data) {
        $dt = new DateTime($data);
        return ($dt->format('w') % 6) == 0;
    }

    public function cancelar() {
        $this->verificar_permissao(array('Coordenador'));

        $reserva = array(
            "resposta" => $this->input->post('resposta'),
            'id_status_reserva' => 3
        );

        $dados_view = array('status' => 'erro', 'msg' => 'Erro ao cancelar reserva. Tente novamente');

        if ($this->reserva->alterar($reserva, $this->input->post('id_reserva'))) {
            $dados_view = array('status' => 'ok', 'msg' => 'Reserva cancelada com sucesso!');
        }

        echo json_encode($dados_view);
    }

    public function cadastrar() {
        $this->verificar_permissao(array('Professor'));

        $dados_view = array('status' => 'erro', 'msg' => 'Erro ao solicitar reserva. Tente novamente');

        if (!$this->isFeriado($this->input->post('data'))) {
            if (!$this->isFinalSemana($this->input->post('data'))) {
                $reserva = array(
                    'id_professor' => $this->session->userdata('id_usuario'),
                    'id_coordenador' => $this->input->post('id_coordenador'),
                    'id_laboratorio' => $this->input->post('id_laboratorio'),
                    'data' => $this->input->post('data'),
                    'hora_inicio' => $this->input->post('hora_inicio'),
                    'hora_fim' => $this->input->post('hora_fim'),
                    'id_status_reserva' => 2
                );

                $dados_view = array('status' => 'erro', 'msg' => 'Erro ao solicitar reserva. Tente novamente');

                if ($this->reserva->cadastrar($reserva)) {
                    $dados_view = array('status' => 'ok', 'msg' => 'Solicitação de reserva realiza com sucesso!');
                }
            } else {
                $dados_view = array('status' => 'erro', 'msg' => 'Não é permitido reserva aos domingos.');
            }
        } else {
            $dados_view = array('status' => 'erro', 'msg' => 'Essa data é feriado.');
        }

        echo json_encode($dados_view);
    }

    public function excluir($id) {
        $this->verificar_permissao(array('Professor'));

        $dados_view = array('status' => 'erro', 'msg' => 'Erro ao excluir reserva. Tente novamente');

        if ($this->reserva->excluir($id)) {
            $dados_view = array('status' => 'ok', 'msg' => 'Reserva excluída com sucesso!');
        }

        echo json_encode($dados_view);
    }

    public function index() {
        $this->verificar_permissao(array('Coordenador'));

        $dados_view['titulo'] = "Reservas de Laboratórios";
        $dados_view['opcao'] = "";
        $dados_view['reservas'] = $this->reserva->buscarTodas($this->session->userdata('id_usuario'));
        $this->carregarPagina('reserva/inicio', $dados_view);
    }

    public function buscarPorStatus() {
        $this->verificar_permissao(array('Coordenador'));

        $id = $this->input->post('id_status_reserva');

        if ($id != "") {
            $dados_view['titulo'] = "Reservas de Laboratórios";
            $dados_view['opcao'] = $id;
            $dados_view['reservas'] = $this->reserva->buscarTodas($this->session->userdata('id_usuario'), $id);
            $this->carregarPagina('reserva/inicio', $dados_view);
        } else {
            redirect(base_url('Reserva'));
        }
    }

    public function abrir($id) {
        $this->verificar_permissao(array('Coordenador'));

        $reserva = $this->reserva->buscarPorId($this->session->userdata('id_usuario'), $id);

        if (count($reserva) == 0) {
            $dados_view['erro'] = "Esta reserva pertence a outro coordenador";
        }

        $dados_view['titulo'] = "Solicitação de Reserva";
        $dados_view['reservas'] = $this->reserva->buscarTodas();
        $dados_view['feriados'] = $this->reserva->buscarFeriados();
        $dados_view['reserva'] = $reserva;
        $this->carregarPagina('reserva/abrir', $dados_view);
    }

    public function paginaAlterar($id) {
        redirect(base_url('Reseva'));
    }

    public function paginaCadastrar() {
        redirect(base_url('Reseva'));
    }

    public function alterar() {
        redirect(base_url('Reseva'));
    }

    public function buscar($id) {
        redirect(base_url('Reseva'));
    }

    public function buscarTodos() {
        redirect(base_url('Reseva'));
    }

}
