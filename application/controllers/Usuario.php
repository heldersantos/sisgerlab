<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'application/controllers/Controller.php';

/**
 * Classe de teste criada para cadastro do primeiro usuário do sistema
 * 
 * @author Helder dos Santos
 */
class Usuario extends Controller {

    public function __construct() {
        parent::__construct();
        redirect(base_url());
        $this->load->model("UsuarioModel", "usuario");
    }

    public function cadastrar() {
        $usuario = array(
            'nome_usuario' => 'Helder dos Santos Sousa',
            'login' => 'administrador@gmail.com',
            'senha' => password_hash('12345678', PASSWORD_DEFAULT),
            'id_tipo_usuario' => 1
        );

        $this->usuario->cadastrar($usuario);
    }

    public function excluir($id) {
        redirect(base_url());
    }

    public function alterar() {
         redirect(base_url());
    }

    public function buscar($id) {
         redirect(base_url());
    }

    public function buscarTodos() {
         redirect(base_url());
    }

    public function index() {
         redirect(base_url());
    }

    public function paginaAlterar($id) {
         redirect(base_url());
    }

    public function paginaCadastrar() {
         redirect(base_url());
    }

}
