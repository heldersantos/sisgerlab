<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'application/controllers/Controller.php';

/**
 * Controlador responsável por receber as requisições referentes ao carregamento de páginas
 * referentes a dados dos coordenadores e de realização do CRUD de coordenadores
 *
 * @author Helder dos Santos
 */
class Coordenador extends Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('UsuarioModel', 'usuario');
    }

    public function alterar() {
        $this->verificar_permissao(array('Administrador'));

        $result = $this->usuario->buscarPorLoginEditar(
                $this->input->post('login'), $this->input->post('id_usuario')
        );

        $dados_view = array('status' => 'erro', 'msg' => 'Erro ao alterar Coordenador');

        if (count($result) === 0) {
            $coordenador = array(
                'nome_usuario' => $this->input->post('nome_usuario'),
                'login' => $this->input->post('login')
            );
            if ($this->usuario->alterar($this->input->post('id_usuario'), $coordenador)) {
                $dados_view = array('status' => 'ok', 'msg' => 'Coordenador alterado com sucesso!');
            }
        } else {
            $dados_view = array('status' => 'erro', 'msg' => 'Este e-mail já é cadastrado!');
        }

        echo json_encode($dados_view);
    }

    public function buscar($id) {
        
    }

    public function buscarTodos() {
        
    }

    public function cadastrar() {
        $this->verificar_permissao(array('Administrador'));
        $coordenador = array(
            'nome_usuario' => $this->input->post('nome_usuario'),
            'login' => $this->input->post('login'),
            'senha' => password_hash($this->input->post('senha'), PASSWORD_DEFAULT),
            'id_tipo_usuario' => 2
        );

        $dadosView = array('status' => 'erro', 'msg' => 'Erro ao cadastrar coordenador!');

        if (count($this->usuario->buscarPorLogin($this->input->post('login'))) == 0) {
            $resposta = $this->usuario->cadastrar($coordenador);

            if ($resposta === true) {
                $dadosView = array('status' => 'ok', 'msg' => 'Coordenador cadastrado com sucesso!');
            }
        } else {
            $dadosView = array('status' => 'erro', 'msg' => 'Este e-mail já é cadastrado!');
        }

        echo json_encode($dadosView);
    }

    public function excluir($id) {
        $this->verificar_permissao(array('Administrador'));
        $dados_view = array("status" => 'erro', "msg" => 'Erro ao excluir coordenador');

        if ($this->usuario->excluir($id)) {
            $dados_view = array("status" => 'ok', "msg" => 'Coordenador excluído com sucesso!');
        }
        echo json_encode($dados_view);
    }

    public function index() {
        $this->verificar_permissao(array('Administrador'));
        $dados_view['titulo'] = "Coordenadores";
        $dados_view['coordenadores'] = $this->usuario->buscarTodosCoordenadores();
        $this->carregarPagina('coordenador/cadastro', $dados_view);
    }

    public function paginaAlterar($id) {
        $this->verificar_permissao(array('Administrador'));
        $dados_view['titulo'] = "Coordenadores";
        $dados_view['coordenador'] = $this->usuario->buscarPorId($id);
        $this->carregarPagina('coordenador/editar', $dados_view);
    }

    public function paginaCadastrar() {
        
    }

}
