<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'application/controllers/Controller.php';

/**
 * Permite que os usuário dos sistema consigam visualizar 
 * seu perfil e fazer alterações de seus dados
 *
 * @author Helder dos Santos
 */
class Perfil extends Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('UsuarioModel', 'usuario');
        $this->verificar_permissao(array('Administrador', 'Professor', 'Coordenador'));
    }

    public function index() {
        $dados_view['titulo'] = "Perfil";
        $this->carregarPagina('perfil', $dados_view);
    }

    public function alterarSenha() {
        $dados_view = array('status' => 'erro', 'msg' => 'Erro ao alterar senha');
        $usuario = $this->usuario->buscarPorId($this->session->userdata('id_usuario'));

        if($this->input->post('nova_senha') === $this->input->post('confirma')) {
            if (count($usuario) > 0) {

                if (password_verify($this->input->post('senha_atual'), $usuario[0]->senha)) {
                    $usuario = array(
                        "senha" => password_hash($this->input->post('nova_senha'), PASSWORD_DEFAULT)
                    );
                    
                    if($this->usuario->alterar($this->session->userdata('id_usuario'), $usuario)){
                        $dados_view = array('status' => 'ok', 'msg' => 'Senha alterada com sucesso!');
                    }else{
                        $dados_view = array('status' => 'erro', 'msg' => 'Erro ao alterar senha!');
                    }
                } else {
                    $dados_view = array('status' => 'erro', 'msg' => 'A senha atual está incorreta!');
                }
            }
        }else{
            $dados_view = array('status' => 'erro', 'msg' => 'Nova senha não confere!');
        }

        echo json_encode($dados_view);
    }

    public function alterarPerfil() {
        $dados_view = array('status' => 'erro', 'msg' => 'Erro ao alterar perfil');
        if (count($this->usuario->buscarAlt($this->input->post('login'))) === 0) {
            $usuario = array(
                "nome_usuario" => $this->input->post('nome_usuario'),
                "login" => $this->input->post('login')
            );

            if ($this->usuario->alterar($this->session->userdata('id_usuario'), $usuario)) {
                $this->session->set_userdata(array('nome' => $this->input->post('nome_usuario'), 'email' => $this->input->post('login')));

                $dados_view = array('status' => 'ok', 'msg' => 'Alteração realizada com sucesso!');
            }
        } else {
            $dados_view = array('status' => 'erro', 'msg' => 'Esse e-mail já é cadastrado!');
        }
        echo json_encode($dados_view);
    }

    public function alterar() {
        
    }

    public function buscar($id) {
        
    }

    public function buscarTodos() {
        
    }

    public function cadastrar() {
        
    }

    public function excluir($id) {
        
    }

    public function paginaAlterar($id) {
        
    }

    public function paginaCadastrar() {
        
    }

}
