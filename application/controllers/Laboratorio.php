<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'application/controllers/Controller.php';

/**
 * Controlador responsável por receber as requisições referentes ao carregamento de páginas
 * referentes a dados dos labratório e de realização do CRUD de laboratório
 * 
 * @author Helder dos Santos
 */
class Laboratorio extends Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('LaboratorioModel', 'laboratorio');
        $this->verificar_permissao(array('Administrador'));
    }

    public function alterar() {
        $dados_view = array('status' => 'erro', 'msg' => 'Erro ao alterar laboratório!');

        $laboratorio = array(
            'nome_laboratorio' => $this->input->post('nome_laboratorio')
        );

        if ($this->laboratorio->alterar($this->input->post('id_laboratorio'), $laboratorio)) {
            $dados_view = array('status' => 'ok', 'msg' => 'Laboratório alterado com sucesso!');
        }

        echo json_encode($dados_view);
    }

    public function buscar($id) {
        
    }

    public function buscarTodos() {
        
    }

    public function cadastrar() {
        $laboratorio = array(
            'nome_laboratorio' => $this->input->post('nome_laboratorio')
        );

        $dadosView = array('status' => 'erro', 'msg' => 'Erro ao cadastrar laboratório!');

        $resposta = $this->laboratorio->cadastrar($laboratorio);

        if ($resposta === true) {
            $dadosView = array('status' => 'ok', 'msg' => 'Laboratorio cadastrado com sucesso!');
        }

        echo json_encode($dadosView);
    }

    public function excluir($id) {
        $dados_view = array("status" => 'erro', "msg" => 'Erro ao excluir laboratório');

        if ($this->laboratorio->excluir($id)) {
            $dados_view = array("status" => 'ok', "msg" => 'Coordenador excluído com sucesso!');
        }
        echo json_encode($dados_view);
    }

    public function index() {
        $dados_view['titulo'] = "Coordenadores";
        $dados_view['laboratorios'] = $this->laboratorio->buscarTodos();
        $this->carregarPagina('laboratorio/cadastro', $dados_view);
    }

    public function paginaAlterar($id) {
        $dados_view['titulo'] = "Laboratórios";
        $dados_view['laboratorio'] = $this->laboratorio->buscarPorId($id);
        $this->carregarPagina('laboratorio/editar', $dados_view);
    }

    public function paginaCadastrar() {
        
    }

}
