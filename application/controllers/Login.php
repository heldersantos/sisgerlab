<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Classe responsável por abrir a janela e realizações a ações 
 * referente a verificação do login do usuário e sair
 *
 * @author Helder dos Santos
 */
class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("UsuarioModel", "usuario");
    }

    public function index() {
        $this->load->view('login');
        if ($this->session->userdata("logado") === true) {
            redirect(base_url());
        }
    }

    /**
     * realiza a criação de sessão para usuários cadastrados no sistema
     */
    public function entrar() {
        if ($this->session->userdata("logado") === true) {
            redirect(base_url());
        }

        $dados_view = array(
            'msg' => 'Usuário ou senha incorreta',
            'status' => 'erro',
            'email' => $this->input->post('email')
        );


        if ($this->input->post('login') == "" || $this->input->post('senha') == "") {
            $dados_view['msg'] = 'Informe usuário e senha';
        } else {
            $usuario = $this->usuario->buscarPorLogin($this->input->post('login'));
            $dados_sessao = null;

            //verifica se encontrou o usuário
            if (count($usuario) > 0) {
                //verifica se a senha informada está correta
                if (password_verify($this->input->post('senha'), $usuario[0]->senha) === true) {

                    $dados_sessao = array(
                        'id_usuario' => $usuario[0]->id_usuario,
                        "nome" => $usuario[0]->nome_usuario,
                        "email" => $usuario[0]->login,
                        "tipo_usuario" => $usuario[0]->nome_tipo_usuario,
                        "logado" => true
                    );

                    if ($usuario[0]->nome_tipo_usuario === 'Professor') {
                        $profCoord = $this->usuario->buscarCoordenadorProf($usuario[0]->id_usuario);
                        $dados_sessao['id_coordenador'] = $profCoord[0]->id_coordenador;
                    }

                    $this->session->set_userdata($dados_sessao);
                    $dados_view['status'] = 'ok';
                }
            }
        }
        echo json_encode($dados_view);
    }

    /**
     * fazer logout no sistema e redirecionar para a tela inicial do sistema
     */
    public function sair() {
        $this->session->sess_destroy();
        redirect(base_url("Login"));
    }

    public function teste() {
        $this->load->view('teste');
    }

}
