<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'application/models/Model.php';

/**
 * Description of UsuarioModel
 *
 * @author Helder dos Santos
 */
class UsuarioModel extends Model {

    public function alterar($id, $valores) {
        $this->db->where('id_usuario', $id);
        return $this->db->update('usuario', $valores);
    }

    public function buscarPorId($id) {
        $this->db->select('*');
        $this->db->from('usuario');
        $this->db->where('id_usuario', $id);
        $this->db->join('tipo_usuario', 'usuario.id_tipo_usuario = tipo_usuario.id_tipo_usuario');
        return $this->db->get()->result();
    }

    public function buscarPorNome($nome) {
        $this->db->select('*');
        $this->db->from('usuario');
        $this->db->where('nome_usuario', $nome);
        $this->db->join('tipo_usuario', 'usuario.id_tipo_usuario = tipo_usuario.id_tipo_usuario');
        return $this->db->get()->result();
    }

    public function buscarPorLogin($login) {
        $this->db->select('*');
        $this->db->from('usuario');
        $this->db->where('login', $login);
        $this->db->join('tipo_usuario', 'usuario.id_tipo_usuario = tipo_usuario.id_tipo_usuario');
        return $this->db->get()->result();
    }

    public function buscarTodos() {
        $this->db->select('*');
        $this->db->from('usuario');
        $this->db->order_by('nome_usuario');
        $this->db->join('tipo_usuario', 'usuario.id_tipo_usuario = tipo_usuario.id_tipo_usuario');
        return $this->db->get()->result();
    }

    public function buscarTodosCoordenadores() {
        $this->db->select('*');
        $this->db->from('usuario');
        $this->db->where('id_usuario !=', $this->session->userdata('id_usuario'));
        $this->db->where('usuario.id_tipo_usuario', 2);
        $this->db->order_by('nome_usuario');
        $this->db->join('tipo_usuario', 'usuario.id_tipo_usuario = tipo_usuario.id_tipo_usuario');
        return $this->db->get()->result();
    }

    public function cadastrar($valores) {
        $this->db->insert('usuario', $valores);
        $idProfessor = $this->db->insert_id();

        $professorCoordenador = array(
            'id_professor' => $idProfessor,
            'id_coordenador' => $this->session->userdata('id_usuario')
        );

        return $this->db->insert('professor_coordenador', $professorCoordenador);
    }

    public function contarTodos() {
        
    }

    public function excluir($id) {
        $this->db->where('id_usuario', $id);
        return $this->db->delete('usuario');
    }

    public function buscarPorLoginEditar($email, $id) {
        $this->db->select('*');
        $this->db->from('usuario');
        $this->db->where('login', $email);
        $this->db->where('id_usuario != ', $id);
        $this->db->join('tipo_usuario', 'usuario.id_tipo_usuario = tipo_usuario.id_tipo_usuario');
        return $this->db->get()->result();
    }

    public function buscarTodosProfessores() {
        $this->db->select('*');
        $this->db->from('usuario');
        $this->db->where('id_usuario !=', $this->session->userdata('id_usuario'));
        $this->db->where('usuario.id_tipo_usuario', 1);
        $this->db->order_by('nome_usuario');
        $this->db->join('tipo_usuario', 'usuario.id_tipo_usuario = tipo_usuario.id_tipo_usuario');
        return $this->db->get()->result();
    }

    public function buscarCoordenadorProf($idProfessor) {
        $this->db->select('*');
        $this->db->where('id_professor', $idProfessor);
        return $this->db->get('professor_coordenador')->result();
    }

    public function buscarAlt($login) {
        $this->db->select('*');
        $this->db->where('id_usuario != ', $this->session->userdata('id_usuario'));
        $this->db->where('login', $login);
        return $this->db->get('usuario')->result();
    }

}
