<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of FeriadoModel
 *
 * @author Helder dos Santos
 */
class FeriadoModel extends CI_Model {
    public function buscarPorData($data){
        $this->db->select('*');
        $this->db->where('data', $data);
        return $this->db->get('feriado')->result();
    }
}
