<?php

/**
 * Description of Model
 *
 * @author Helder dos Santos
 */
abstract class Model extends CI_Model{
    public function __construct() {
        parent::__construct();
    }
    
    abstract function buscarTodos();
    abstract function buscarPorId($id);
    abstract function buscarPorNome($nome);
    abstract function alterar($id, $valores);
    abstract function cadastrar($valores);
    abstract function excluir($id);
    abstract function contarTodos();
}
