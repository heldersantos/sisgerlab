<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of ReservaModel
 *
 * @author Helder dos Santos
 */
class ReservaModel extends CI_Model {

    public function cadastrar($reserva) {
        return $this->db->insert('reserva', $reserva);
    }

    public function buscarTodas($idCoordenador = 0, $statusReseva = null) {
        $this->db->select('*');
        $this->db->from('reserva');
        $this->db->join('status_reserva', 'reserva.id_status_reserva = status_reserva.id_status_reserva');
        $this->db->join('usuario', 'usuario.id_usuario = reserva.id_professor');
        $this->db->join('laboratorio', 'laboratorio.id_laboratorio = reserva.id_laboratorio');
        $this->db->order_by('reserva.data', 'desc');

        if ($idCoordenador != 0) {
            $this->db->where('id_coordenador', $idCoordenador);
        }
        if ($statusReseva != null) {
            $this->db->where('reserva.id_status_reserva', $statusReseva);
        }
        return $this->db->get()->result();
    }

    public function buscarAprovadas() {
        $this->db->select('*');
        $this->db->from('reserva');
        $this->db->join('status_reserva', 'reserva.id_status_reserva = status_reserva.id_status_reserva');
        $this->db->join('usuario', 'usuario.id_usuario = reserva.id_professor');
        $this->db->join('laboratorio', 'laboratorio.id_laboratorio = reserva.id_laboratorio');
        $this->db->where('nome_status_reserva', 'Aprovada');
        return $this->db->get()->result();
    }

    public function buscarPorId($idCoordenador, $idReserva) {
        $this->db->select('*');
        $this->db->from('reserva');
        $this->db->join('status_reserva', 'reserva.id_status_reserva = status_reserva.id_status_reserva');
        $this->db->join('usuario', 'usuario.id_usuario = reserva.id_professor');
        $this->db->join('laboratorio', 'laboratorio.id_laboratorio = reserva.id_laboratorio');
        $this->db->where('id_coordenador', $idCoordenador);
        $this->db->where('id_reserva', $idReserva);
        return $this->db->get()->result();
    }

    public function buscarTodasProfessor() {
        $this->db->select('*');
        $this->db->from('reserva');
        $this->db->order_by('reserva.data', 'desc');
        $this->db->join('status_reserva', 'reserva.id_status_reserva = status_reserva.id_status_reserva');
        $this->db->join('usuario', 'usuario.id_usuario = reserva.id_professor');
        $this->db->join('laboratorio', 'laboratorio.id_laboratorio = reserva.id_laboratorio');
        $this->db->where('id_professor', $this->session->userdata('id_usuario'));
        return $this->db->get()->result();
    }

    public function buscarFeriados() {
        $this->db->select('*');
        return $this->db->get('feriado')->result();
    }

    public function alterar($reserva, $idReserva) {
        $this->db->where('id_reserva', $idReserva);
        return $this->db->update('reserva', $reserva);
    }

    public function excluir($id) {
        $this->db->where('id_reserva', $id);
        return $this->db->delete('reserva');
    }

}
