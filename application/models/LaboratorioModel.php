<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'application/models/Model.php';

/**
 * Description of LaboratorioModel
 *
 * @author Helder dos Santos
 */
class LaboratorioModel extends Model{
    //put your code here
    public function alterar($id, $valores) {
        $this->db->where('id_laboratorio', $id);
        return $this->db->update('laboratorio', $valores);
    }

    public function buscarPorId($id) {
        $this->db->select('*');
        $this->db->where('id_laboratorio', $id);
        return $this->db->get('laboratorio')->result();
    }

    public function buscarPorNome($nome) {
        
    }

    public function buscarTodos() {
        $this->db->select('*');
        return $this->db->get('laboratorio')->result();
    }

    public function cadastrar($valores) {
        return $this->db->insert('laboratorio', $valores);
    }

    public function contarTodos() {
        
    }

    public function excluir($id) {
        $this->db->where('id_laboratorio', $id);
        return $this->db->delete('laboratorio');
    }

}
