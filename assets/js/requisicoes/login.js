$(document).ready(function () {
    $("#form-login").submit(function (event) {
        dados = {
            login: $("#login").val(),
            senha: $("#senha").val()
        };

        $.ajax({
            method: "POST",
            url: base_url + "Login/entrar",
            data: dados,
            dataType: "json"
        }).done(function (resposta) {
            if (resposta.status === "ok") {
               window.location.href = base_url;
            } else {
               mostraDialogo(resposta.msg, "danger", 3000);
            }

        }).fail(function (jqXHR, textStatus) {
            mostraDialogo("Algo de errado aconteceu!", "danger", 3000);
        });
        event.preventDefault();
    });
});




