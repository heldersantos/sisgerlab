$(document).ready(function () {
    $("#cad-coordenador").submit(function (event) {
        dados = {
            nome_usuario: $("#nome_usuario").val(),
            login: $("#login").val(),
            senha: $("#senha").val()
        };

        $.ajax({
            method: "POST",
            url: base_url + "Coordenador/cadastrar",
            data: dados,
            dataType: "json"
        }).done(function (resposta) {
            console.log(resposta);
            if (resposta.status === "ok") {
                mostraDialogo(resposta.msg, "success", 3000);
                setTimeout(function () {
                    window.location.href = base_url + "Coordenador";
                }, 2000);
            } else {
                mostraDialogo(resposta.msg, "danger", 3000);
            }

        }).fail(function (jqXHR, textStatus) {
            mostraDialogo("Algo de errado aconteceu!", "danger", 3000);
        });
        event.preventDefault();
    });





    $("#edit-coordenador").submit(function (event) {
        dados = {
            id_usuario: $("#id_usuario").val(),
            nome_usuario: $("#nome_usuario").val(),
            login: $("#login").val(),
            senha: $("#senha").val()
        };

        $.ajax({
            method: "POST",
            url: base_url + "Coordenador/alterar",
            data: dados,
            dataType: "json"
        }).done(function (resposta) {
            console.log(resposta);
            if (resposta.status === "ok") {
                mostraDialogo(resposta.msg, "success", 3000);
                setTimeout(function () {
                    window.location.href = base_url + "Coordenador";
                }, 2000);
            } else {
                mostraDialogo(resposta.msg, "danger", 3000);
            }

        }).fail(function (jqXHR, textStatus) {
            mostraDialogo("Algo de errado aconteceu!", "danger", 3000);
        });
        event.preventDefault();
    });
});

function excluir(id, coordenador) {
    swal({
        title: "Atenção",
        text: "Deseja realmente excluir o coordeandor " + coordenador,
        icon: "warning",
        buttons: ['Cancelar', 'Confirmar'],
        dangerMode: true
    }).then((willDelete) => {
        if (willDelete) {

            $.ajax({
                method: "POST",
                url: base_url + "Coordenador/excluir/" + id,
                data: {},
                dataType: "json"
            }).done(function (resposta) {
                console.log(resposta);
                if (resposta.status === "ok") {
                    mostraDialogo(resposta.msg, "success", 3000);
                    setTimeout(function () {
                        window.location.href = base_url + "Coordenador";
                    }, 2000);
                } else {
                    mostraDialogo(resposta.msg, "danger", 3000);
                }

            }).fail(function (jqXHR, textStatus) {
                mostraDialogo("Algo de errado aconteceu!", "danger", 3000);
            });
        }
    });
}
