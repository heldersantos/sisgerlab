$(document).ready(function () {
    $("#cad-laboratorio").submit(function (event) {
        dados = {
            nome_laboratorio: $("#nome_laboratorio").val()
        };

        $.ajax({
            method: "POST",
            url: base_url + "Laboratorio/cadastrar",
            data: dados,
            dataType: "json"
        }).done(function (resposta) {
            console.log(resposta);
            if (resposta.status === "ok") {
                mostraDialogo(resposta.msg, "success", 3000);
                setTimeout(function () {
                    window.location.href = base_url + "Laboratorio";
                }, 2000);
            } else {
                mostraDialogo(resposta.msg, "danger", 3000);
            }

        }).fail(function (jqXHR, textStatus) {
            mostraDialogo("Algo de errado aconteceu!", "danger", 3000);
        });
        event.preventDefault();
    });





    $("#edit-laboratorio").submit(function (event) {
        dados = {
            id_laboratorio: $("#id_laboratorio").val(),
            nome_laboratorio: $("#nome_laboratorio").val()
        };

        $.ajax({
            method: "POST",
            url: base_url + "Laboratorio/alterar",
            data: dados,
            dataType: "json"
        }).done(function (resposta) {
            console.log(resposta);
            if (resposta.status === "ok") {
                mostraDialogo(resposta.msg, "success", 3000);
                setTimeout(function () {
                    window.location.href = base_url + "Laboratorio";
                }, 2000);
            } else {
                mostraDialogo(resposta.msg, "danger", 3000);
            }

        }).fail(function (jqXHR, textStatus) {
            mostraDialogo("Algo de errado aconteceu!", "danger", 3000);
        });
        event.preventDefault();
    });
});

function excluir(id, coordenador) {
    swal({
        title: "Atenção",
        text: "Deseja realmente excluir o laboratorio " + coordenador,
        icon: "warning",
        buttons: ['Cancelar', 'Confirmar'],
        dangerMode: true
    }).then((willDelete) => {
        if (willDelete) {

            $.ajax({
                method: "POST",
                url: base_url + "Laboratorio/excluir/" + id,
                data: {},
                dataType: "json"
            }).done(function (resposta) {
                console.log(resposta);
                if (resposta.status === "ok") {
                    mostraDialogo(resposta.msg, "success", 3000);
                    setTimeout(function () {
                        window.location.href = base_url + "Laboratorio";
                    }, 2000);
                } else {
                    mostraDialogo(resposta.msg, "danger", 3000);
                }

            }).fail(function (jqXHR, textStatus) {
                mostraDialogo("Algo de errado aconteceu!", "danger", 3000);
            });
        }
    });
}
