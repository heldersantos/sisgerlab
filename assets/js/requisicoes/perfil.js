var divSelecionada = 'dados';

$(document).ready(function () {
    $("#form-perfil").submit(function (event) {
        dados = {
            nome_usuario: $("#nome_usuario").val(),
            login: $("#login").val()
        };

        $.ajax({
            method: "POST",
            url: base_url + "Perfil/alterarPerfil",
            data: dados,
            dataType: "json"
        }).done(function (resposta) {
            console.log(resposta);
            if (resposta.status === "ok") {
                mostraDialogo(resposta.msg, "success", 3000);
                setTimeout(function () {
                    window.location.href = base_url + "Perfil";
                }, 2000);
            } else {
                mostraDialogo(resposta.msg, "danger", 3000);
            }

        }).fail(function (jqXHR, textStatus) {
            mostraDialogo("Algo de errado aconteceu!" + textStatus, "danger", 3000);
        });
        event.preventDefault();
    });


    $("#form-senha").submit(function (event) {
        dados = {
            senha_atual: $("#senha_atual").val(),
            nova_senha: $("#nova_senha").val(),
            confirma: $("#confirmar").val()
        };

        $.ajax({
            method: "POST",
            url: base_url + "Perfil/alterarSenha",
            data: dados,
            dataType: "json"
        }).done(function (resposta) {
            console.log(resposta);
            if (resposta.status === "ok") {
                mostraDialogo(resposta.msg, "success", 3000);
                setTimeout(function () {
                    window.location.href = base_url + "Perfil";
                }, 2000);
            } else {
                mostraDialogo(resposta.msg, "danger", 3000);
            }

        }).fail(function (jqXHR, textStatus) {
            console.log(jqXHR);
            mostraDialogo("Algo de errado aconteceu!", "danger", 3000);
        });
        event.preventDefault();
    });

    $('#dados').addClass('visivel');

    $('#links-perfil a').click(function (i) {
        $(this).val();
        $('a').removeClass('selecionado');
        $(this).removeClass('naoselecionado');
        $(this).addClass('selecionado');

        if (divSelecionada === "dados") {
            $('#dados').removeClass('visivel');
            $('#senha').addClass('visivel');
            divSelecionada = "senha";
        } else {
            $('#dados').addClass('visivel');
            $('#senha').removeClass('visivel');
            divSelecionada = "dados";
        }
    });
});

function mudar(valor) {
    divSelecionada = valor;
}


