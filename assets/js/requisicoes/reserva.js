var mensagem = "";
$(document).ready(function () {
    $("#hora_inicio").html("");
    $("#hora_fim").html("");
    $("#hora_inicio").change(function (event) {
        hora = $("#hora_inicio").val().split("-");
        hora = hora[0];
        html = "<option value=\"\">Selecione</option>";
        for (i = parseInt(hora) + 1; i < 23; i++) {
            html += "<option value=\"" + (i < 10 ? "0" : "") + i + ":00:00\">" + (i < 10 ? "0" : "") + i + ":00</option>";
        }

        $("#hora_fim").html(html);
        event.preventDefault();
    });
    $("#data").change(function (event) {

        html = "";
        dtCompleta = new Date();
        dtAtual = dtCompleta.getFullYear() + "-" + (dtCompleta.getMonth() + 1) + "-" + dtCompleta.getDate();
        hora = "07";
        if ($("#data").val() === dtAtual) {
            if (parseInt(new Date().getHours().toLocaleString()) >= 7) {
                hora = new Date().getHours().toLocaleString();
            }
        }

        html = "<option value=\"\">Selecione</option>";
        for (i = parseInt(hora); i < 23; i++) {
            html += "<option value=\"" + (i < 10 ? "0" : "") + i + ":00:00\">" + (i < 10 ? "0" : "") + i + ":00</option>";
        }

        $("#hora_inicio").html(html);
        event.preventDefault();
    });
    $("#cad-reserva").submit(function (event) {
        if (verificarData($("#data").val())) {
            dados = {
                id_laboratorio: $("#id_laboratorio").val(),
                data: $("#data").val(),
                hora_inicio: $("#hora_inicio").val(),
                hora_fim: $("#hora_fim").val(),
                id_coordenador: $("#id_coordenador").val()
            };
            console.log(dados);
            $.ajax({
                method: "POST",
                url: base_url + "Reserva/cadastrar",
                data: dados,
                dataType: "json"
            }).done(function (resposta) {
                console.log(resposta);
                if (resposta.status === "ok") {
                    mostraDialogo(resposta.msg, "success", 3000);
                    setTimeout(function () {
                        window.location.href = base_url;
                    }, 2000);
                } else {
                    mostraDialogo(resposta.msg, "danger", 3000);
                }

            }).fail(function (jqXHR, textStatus) {
                mostraDialogo("Algo de errado aconteceu!", "danger", 3000);
            });
        } else {
            mostraDialogo("Esta data já passou!", "danger", 3000);
        }
        event.preventDefault();
    });
});
/**
 * Envia uma requisição AJAX para aprovar uma reserva e recebe um JSON
 * como resposta dos status da aprovação da reserva
 * @param {type} id
 * @return {undefined}
 */
function aprovarReserva(id) {
    if (verificarData($("#data").val())) {
        console.log();
        if (verificarHora($("#hora_inicio").val(), $("#hora_fim").val())) {
            dados = {
                data: $('#data').val(),
                hora_inicio: $('#hora_inicio').val(),
                hora_fim: $('#hora_fim').val(),
                id_reserva: id
            };
            $.ajax({
                method: "POST",
                url: base_url + "Reserva/aprovar",
                data: dados,
                dataType: "json"
            }).done(function (resposta) {
                console.log(resposta);
                if (resposta.status === "ok") {
                    mostraDialogo(resposta.msg, "success", 3000);
                    setTimeout(function () {
                        window.location.href = base_url + "Reserva";
                    }, 2000);
                } else {
                    mostraDialogo(resposta.msg, "danger", 3000);
                }

            }).fail(function (jqXHR, textStatus) {
                mostraDialogo("Algo de errado aconteceu. Não é permitido solicitar a reserva", "danger", 3000);
            });
        } else {
            if (mensagem === "") {
                mostraDialogo("<strong>Horário não permitido</strong>, <br>A hora final deve ser maior que a inicial em pelo menos 1 hora!", "danger", 4000);
            } else {
                mostraDialogo(mensagem, "danger", 4000);
                mensagem = "";
            }
        }
    } else {
        mostraDialogo("Esta data já passou!", "danger", 3000);
    }
}

function verificarData(novaData) {
    valido = false;
    dtCompleta = new Date();
    dtAtual = dtCompleta.getFullYear() + "-" + (dtCompleta.getMonth() + 1) + "-" + dtCompleta.getDate();
    if (novaData === dtAtual) {
        valido = true;
    } else {
        novaData = novaData.split('-');
        novaData = new Date(novaData[0], novaData[1] - 1, novaData[2]);
        console.log(novaData);
        console.log(dtCompleta);
        console.log(novaData.toDateString());
        console.log(dtCompleta.toDateString());
        if (novaData > dtCompleta) {
            valido = true;
        }
    }

    return valido;
}

/**
 * Verifica se a hora inicial é maior que a final, caso afirmativa,
 * retorna false, caso contrário retorna veradeiro
 * @param {type} horaInicio
 * @param {type} horaFim
 * @return {Boolean}
 */
function verificarHora(horaInicio, horaFim) {
    dtCompleta = new Date();
    dtAtual = dtCompleta.getFullYear() + "-" + (dtCompleta.getMonth() + 1) + "-" + dtCompleta.getDate();
    
    if ($("#data").val() === dtAtual) {
        h = parseInt(parseInt(horaInicio.split(":")[0]));
        if (parseInt(new Date().getHours().toLocaleString()) >= h) {
            mensagem = "Esta hora já passou";
            return false;
        }
    }

    var a = moment('21/12/2016 ' + horaInicio, 'DD/MM/YYYY hh:mm:ss');
    var b = moment('21/12/2016 ' + horaFim, 'DD/MM/YYYY hh:mm:ss');
    var diferenca = b.diff(a, 'hours');
    if (diferenca < 1) {
        return false;
    }

    horaInicio = parseInt(horaInicio.split(":")[0]);
    horaFim = parseInt(horaFim.split(":")[0]);
    if (parseInt(horaInicio) >= parseInt(horaFim)) {
        return false;
    }

    return true;
}


function cancelarReserva(id) {
    swal("Informa o motivo do cancelamento desta reseva", {
        content: "input"
    }).then((value) => {
        dados = {
            resposta: value,
            id_reserva: id
        };
        $.ajax({
            method: "POST",
            url: base_url + "Reserva/cancelar",
            data: dados,
            dataType: "json"
        }).done(function (resposta) {
            console.log(resposta);
            if (resposta.status === "ok") {
                mostraDialogo(resposta.msg, "success", 3000);
                setTimeout(function () {
                    window.location.href = base_url + "Reserva";
                }, 2000);
            } else {
                mostraDialogo(resposta.msg, "danger", 3000);
            }

        }).fail(function (jqXHR, textStatus) {
            mostraDialogo("Algo de errado aconteceu!", "danger", 3000);
        });
    });
}


function excluirReserva(url) {

    swal({
        title: "Atenção",
        text: "Deseja realmente excluir esta reserva?",
        icon: "warning",
        buttons: ['Cancelar', 'Confirmar'],
        dangerMode: true
    }).then((willDelete) => {
        if (willDelete) {
            $.ajax({
                method: "POST",
                url: url,
                data: {},
                dataType: "json"
            }).done(function (resposta) {
                console.log(resposta);
                if (resposta.status === "ok") {
                    mostraDialogo(resposta.msg, "success", 3000);
                    setTimeout(function () {
                        window.location.href = base_url;
                    }, 2000);
                } else {
                    mostraDialogo(resposta.msg, "danger", 3000);
                }

            }).fail(function (jqXHR, textStatus) {
                mostraDialogo("Algo de errado aconteceu!", "danger", 3000);
            });
        }
    });
}


