###################
O que é o SISGERLAB?
###################

O SisgerLab é um sistema construído como requisito para obtenção
da nota de qualidade de software da pós graduação em Engenharia de Software,
ele serve para auxiliar coordenadores de cursos 
a gerenciar as solicitações de reservas de laboratório feita pelos professores.
Ele foi construído utilizando o framework PHP Codeigniter

*******************
Como acessar ele?
*******************

Para ver o sistema funcionando basta abrir o link http://www.seedstech.com.br/sisgerlab

*******************
Quais arquivos foram criados?
*******************

TODO O CÓDIGO CONSTRUÍDO ESTÁ DENTRO DOS DIRETÓRIOS (TODOS O RESTANTE PERTENCE AO FRAMEWORK):

- application/controllers
- application/helpers
- application/modelos
- application/views
- assets
