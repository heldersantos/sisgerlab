-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 29-Nov-2018 às 18:31
-- Versão do servidor: 10.1.34-MariaDB
-- PHP Version: 7.1.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbsisgerlab`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `feriado`
--

CREATE TABLE `feriado` (
  `id_feriado` int(11) NOT NULL,
  `data` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `feriado`
--

INSERT INTO `feriado` (`id_feriado`, `data`) VALUES
(1, '01-01'),
(2, '04-21'),
(3, '05-01'),
(4, '09-07'),
(5, '10-12'),
(6, '11-02'),
(7, '11-15'),
(8, '12-25');

-- --------------------------------------------------------

--
-- Estrutura da tabela `laboratorio`
--

CREATE TABLE `laboratorio` (
  `id_laboratorio` int(11) NOT NULL,
  `nome_laboratorio` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `laboratorio`
--

INSERT INTO `laboratorio` (`id_laboratorio`, `nome_laboratorio`) VALUES
(3, 'Laboratório de Informática'),
(4, 'Laboratório de Línguas'),
(5, 'Laboratório de Química'),
(6, 'Laboratório de Biologia');

-- --------------------------------------------------------

--
-- Estrutura da tabela `professor_coordenador`
--

CREATE TABLE `professor_coordenador` (
  `id_professor_coordenador` int(11) NOT NULL,
  `id_professor` int(11) NOT NULL,
  `id_coordenador` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `professor_coordenador`
--

INSERT INTO `professor_coordenador` (`id_professor_coordenador`, `id_professor`, `id_coordenador`) VALUES
(1, 20, 21),
(2, 24, 21),
(3, 25, 13),
(4, 26, 21),
(5, 26, 21),
(6, 27, 21);

-- --------------------------------------------------------

--
-- Estrutura da tabela `reserva`
--

CREATE TABLE `reserva` (
  `id_reserva` int(11) NOT NULL,
  `id_professor` int(11) NOT NULL,
  `data` date NOT NULL,
  `hora_inicio` time NOT NULL,
  `id_status_reserva` int(11) NOT NULL,
  `id_laboratorio` int(11) NOT NULL,
  `hora_fim` time NOT NULL,
  `resposta` varchar(255) DEFAULT NULL,
  `id_coordenador` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `reserva`
--

INSERT INTO `reserva` (`id_reserva`, `id_professor`, `data`, `hora_inicio`, `id_status_reserva`, `id_laboratorio`, `hora_fim`, `resposta`, `id_coordenador`) VALUES
(20, 26, '2018-11-24', '14:00:00', 1, 6, '15:00:00', NULL, 21),
(22, 20, '2018-11-28', '09:00:00', 2, 3, '12:00:00', NULL, 21),
(23, 27, '2018-12-04', '07:00:00', 2, 3, '10:00:00', NULL, 21);

-- --------------------------------------------------------

--
-- Estrutura da tabela `status_reserva`
--

CREATE TABLE `status_reserva` (
  `id_status_reserva` int(11) NOT NULL,
  `nome_status_reserva` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `status_reserva`
--

INSERT INTO `status_reserva` (`id_status_reserva`, `nome_status_reserva`) VALUES
(1, 'Aprovada'),
(2, 'Pendente'),
(3, 'Cancelada');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipo_usuario`
--

CREATE TABLE `tipo_usuario` (
  `id_tipo_usuario` int(11) NOT NULL,
  `nome_tipo_usuario` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tipo_usuario`
--

INSERT INTO `tipo_usuario` (`id_tipo_usuario`, `nome_tipo_usuario`) VALUES
(1, 'Professor'),
(2, 'Coordenador'),
(3, 'Administrador');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL,
  `nome_usuario` varchar(60) NOT NULL,
  `login` varchar(60) NOT NULL,
  `senha` varchar(65) NOT NULL,
  `id_tipo_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `nome_usuario`, `login`, `senha`, `id_tipo_usuario`) VALUES
(13, 'Helder dos Santos Sousa', 'administrador@gmail.com', '$2y$10$OOu6I6CWXJYM9ElIpV1YP.7C0zUGnkUYGigvcKbwEcfcrbeXaiq8W', 3),
(21, 'Mateus Dias da Silva', 'coordenador@gmail.com', '$2y$10$GVjbKBotRkHbFMGGffJQBeapXloDRRLzV.Ojc./fBpIjQ5FTqB3F2', 2),
(22, 'Sophia Costa Martins', 'coordenadora@gmail.com', '$2y$10$hG4zw3Szyh9CWHg7JZ/MJu3BKLuRPlZ1nWlfpMDBQpJOxTOMSd0cq', 2),
(27, 'Marcos Dias Pereira', 'professor@gmail.com', '$2y$10$.h1yiJsD.qiI02jvCDf1GuLjshzuTi1NzQ.qTWSY/rBXPt2Udib3O', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `feriado`
--
ALTER TABLE `feriado`
  ADD PRIMARY KEY (`id_feriado`);

--
-- Indexes for table `laboratorio`
--
ALTER TABLE `laboratorio`
  ADD PRIMARY KEY (`id_laboratorio`);

--
-- Indexes for table `professor_coordenador`
--
ALTER TABLE `professor_coordenador`
  ADD PRIMARY KEY (`id_professor_coordenador`);

--
-- Indexes for table `reserva`
--
ALTER TABLE `reserva`
  ADD PRIMARY KEY (`id_reserva`);

--
-- Indexes for table `status_reserva`
--
ALTER TABLE `status_reserva`
  ADD PRIMARY KEY (`id_status_reserva`);

--
-- Indexes for table `tipo_usuario`
--
ALTER TABLE `tipo_usuario`
  ADD PRIMARY KEY (`id_tipo_usuario`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `feriado`
--
ALTER TABLE `feriado`
  MODIFY `id_feriado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `laboratorio`
--
ALTER TABLE `laboratorio`
  MODIFY `id_laboratorio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `professor_coordenador`
--
ALTER TABLE `professor_coordenador`
  MODIFY `id_professor_coordenador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `reserva`
--
ALTER TABLE `reserva`
  MODIFY `id_reserva` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `status_reserva`
--
ALTER TABLE `status_reserva`
  MODIFY `id_status_reserva` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tipo_usuario`
--
ALTER TABLE `tipo_usuario`
  MODIFY `id_tipo_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
